// tool to merge webpack configs
const merge = require('webpack-merge');
// minifier
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
// common config
const commonConfig = require('./webpack.common.js');

module.exports = merge(commonConfig, {
   plugins: [
     new UglifyJSPlugin({
         parallel: true, 
         ecma: 6
    })
   ]
});
