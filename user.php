<?php
require("init.php");

$userObj = new user();
/*
 * VIEW ROUTES
 */

// index route, render the basic html view
$router->get("/", function () {
    //$template->display("userlist.tpl");

});

// index route, render the basic html view
$router->any("/add", function () use ($userObj) {
    //$template->display("userlist.tpl");
    $userId = $userObj->add("test", "test@test.de", "test");
    $user = $userObj->getUser($userId);

    echo "<pre>";
    print_r($user);
});

$router->post("/login", function ($request, $response, $args) use ($userObj) {
    //get XSS sanitized POST arguments
    $cleanPost = $_POST;
    print_r($cleanPost);
    if($userObj->login($cleanPost["email"], $cleanPost["password"])) {
        return $response->withStatus(301)->withHeader("Location", "http://127.0.0.1/cherrycake/index.php");
    }
});
$router->get("/login/{email}/{password}", function ($request, $response, $args) use ($userObj) {
    //get XSS sanitized POST arguments
    $cleanArgs = $args;
;
    return json_encode($userObj->login($cleanArgs["email"], $cleanArgs["password"]));
});

$router->get("/logout", function($request, $response, $args) use ($userObj) {
     if($userObj->logout()) {
         return $response->withStatus(301)->withHeader("Location", "http://127.0.0.1/cherrycake/index.php");
     }
});


$router->run();