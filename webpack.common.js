const path = require('path');
/*
 * entry: entry path for the module. for each module a JS bundle is created in /js/[module]/[module].js
 * 
 */
module.exports = {
    entry: {
        cherrycake: "./include/js/webpackBundle.js"
    },
    output: {
        filename: './include/js/bundle/[name].js',
        path: path.resolve(__dirname, '')
    },
    module: {
        rules: [
            {
                test: /\.js$/
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
                        // the "scss" and "sass" values for the lang attribute to the right configs here.
                        // other preprocessors should work out of the box, no loader config like this necessary.
                        'scss': [
                            'vue-style-loader',
                            'css-loader',
                            'sass-loader'
                        ]
                    }
                    // other vue-loader options go here
                }
            }
        ]
    },
    plugins: []
};
