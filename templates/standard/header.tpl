{config_load file="lng.conf" scope="global" }
<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{$title}</title>
    <link href="templates/standard/css/cherrycake.css" rel="stylesheet">

    {* JS code *}
    <script type="text/javascript" src="include/js/lib/vue.dev.js"></script>
    <script type="text/javascript" src="include/js/viewHelper.js"></script>

    <script type="text/javascript" src="include/js/bundle/cherrycake.js"></script>
</head>

<body>