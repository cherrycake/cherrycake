<h3>Form</h3>

<form method="post" action="#" >
    <div class="form-group form-row">
        <div class="col-md-11">
            <label for="field1">Feld 1:</label>
            <input id="field1" type="text" class="form-control" placeholder="Enter Value"
                   required minlength="3"
                   autocomplete="nope">
        </div>
        <div class="col-md-1">
            <span id="field1_error" data-balloon="Error ! At least 3 characters." data-balloon-pos="right"></span>
        </div>


    </div>

    <div class="form-group form-row">
        <div class="col-md-11">
            <label for="exampleInputPassword1">Password</label>
            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password"
                   autocomplete="off"
                   required
                   data-custom-validator="validator1"
                    >
        </div>

        <div class="col-md-1">
            <p class="bg-info" id="exampleInputPassword1_error" data-balloon="Error ! Enter password." data-balloon-pos="right"></p>
        </div>

    </div>

    <div class="form-check">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input">
            Check me out
        </label>
    </div>

    <div class="form-group form-row">
        <div class="col-md-11">
            <label for="exampleSelect">select</label>

            <select class="form-control" id="exampleSelect" required>
                <option value="">1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </select>
        </div>
        <div class="col-md-1">
            <p id="exampleSelect_error" data-balloon="Error ! Select value." data-balloon-pos="right"></p>
        </div>
    </div>

    <div class="form-group form-row">
        <div class="col-md-11">
            <label for="exampleSelect2">multiple select</label>

            <select multiple class="form-control" id="exampleSelect2" required >
                <option disabled selected>Please select</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </select>
        </div>
        <div class="col-md-1">
            <p id="exampleSelect2_error" data-balloon="Error ! Select value." data-balloon-pos="right"></p>
        </div>

    </div>

    <div class="form-group form-row">
        <div class="col-md-11">
            <label for="exampleTextarea">textarea</label>
            <textarea class="form-control" id="exampleTextarea" rows="3" required=""></textarea>
        </div>
        <div class="col-md-1">
            <p id="exampleTextarea_error" data-balloon="Error !" data-balloon-pos="right"></p>
        </div>


    </div>

    <div class="form-group">
        <label for="exampleFile">Example file input</label>
        <input type="file" class="form-control-file" id="exampleFile">
    </div>

    <div class="form-group form-row">
        <div class="col-md-11">
            <div class="form-check">
                <label class="form-check-label">
                    <input id="exampleCheckbox" class="form-check-input" type="checkbox" value="" required="">
                    Option one is this and that&mdash;be sure to include why it's great
                </label>
            </div>
        </div>
        <div class="col-md-1">
            <p id="exampleCheckbox_error" data-balloon="Error ! Check this box" data-balloon-pos="right"></p>
        </div>

    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
    <button type="reset" class="btn btn-default">Cancel</button>
</form>