{include file="header.tpl" title="Index"}
<div class="container-fluid" id="firstView">
    <div class="row">

        <div class="col-md-8">
            <button class="btn btn-primary">
                <span>
                    Button Text
                </span>
                <loader block="firstView" indicator="loaders/spinning-arrow.svg" class="display-inline animated-loader"></loader>
            </button>

            <br/><br/>

            {literal}
                <index-list :items="items"></index-list>
            {/literal}
        </div>

        <div class="col-md-4">
            {if $loggedin}
                {include file="navbarOn.tpl"}
            {else}
                {include file="login.tpl"}
            {/if}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <h3>Buttons</h3>

            <!-- basic loading indicator -->
            <button class="btn btn-warning">
                <loader block="testButton" indicator="loaders/spinning-arrow.svg"></loader>
                <span>
                     Button Text
                </span>
            </button>

            <!-- always visible -->
            <button id="testButton" class="btn btn-info">
                <loader block="testButton" indicator="loaders/spinning-arrow.svg" class="display-inline"></loader>
                <span>
                     Button Text
                </span>
            </button>

            <!-- loader animated in when the button is clicked. loader left of button text -->
            <button id="testButton2" class="btn btn-danger" onclick="toggleLoadingIndicator(this.id)">
                <loader block="testButton2" indicator="loaders/spinning-arrow.svg" class="display-inline animated-loader"></loader>
                <span>
                     Button Text
                </span>
            </button>


            <!-- loader animated in when the button is clicked. loader right of button text -->
            <button id="testButton3" class="btn btn-success" onclick="toggleLoadingIndicator('testButton3')">
                <span>
                    Button Text
                </span>
                <loader block="testButton3" indicator="loaders/spinning-arrow.svg" class="display-inline animated-loader"></loader>
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            {include file="form.tpl"}
        </div>
    </div>
</div>
<script type="text/javascript" src="include/js/index.js"></script>
{include file="footer.tpl"}
