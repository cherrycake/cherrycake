<div id="login" style="background-color: #ec6a58">
    <h3>Login</h3>
    <form method="post" action="user.php/login" id="loginForm">
        <div class="form-group">
            <label for="email">E-Mail</label>
            <input id="email" name="email" type="text" class="form-control" placeholder="Enter Email" required />
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input id="password" name="password" type="password" class="form-control"  placeholder="Password" required />
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-default">Cancel</button>
    </form>
</div>
