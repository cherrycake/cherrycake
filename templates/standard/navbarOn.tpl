<nav class="cl-effect-13 margin-top-ten-px">
    <ul class="nav nav-pills float-left">
        <li class="nav-item">
            <a class="nav-link active" href="#">Home</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#">Profile</a>
        </li>

        <li class="nav-item">
            <a class="nav-link disabled" href="#">Disabled link</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#">Messages</a>
        </li>
    </ul>
    <div>
        <button class="btn btn-sm btn-danger float-right" onclick="logout()">
            <i class="feather icon-radio font-weight-bold"></i>
        </button>
    </div>
</nav>
