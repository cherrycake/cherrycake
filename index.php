<?php
require("init.php");

/*
 * VIEW ROUTES
 */

// index route, render the basic html view
$router->get("/", function ($request, $response, $args) {
    return $this->template->display("index.tpl");
});

$router->get("/getData", function ($request, $response, $args) {
    $myData = ["items" => [
        ["username" => "Eva",
            "accountStatus" => "rich"],
        ["username" => "Philipp",
            "accountStatus" => "rich"],
        ["username" => "Tobias",
            "accountStatus" => "poor"],
        ["username" => "Denise",
            "accountStatus" => "nerd"],
    ],
        ["count" => 0]
    ];

    sleep(1);
    return json_encode($myData);
});

/*
 * DATA ROUTES
 */
$router->get("/validate/[{input}]", function ($request, $response, $args) {
    $valid = true;
    $errorText = [];
    $input = $args["input"];

    if(!$input)
    {
        return json_encode(false);
    }
    if(!stristr($input, "j"))
    {
        $valid = false;
        array_push($errorText, "Needs more J!");
    }
    if(!stristr($input, "k"))
    {
        $valid = false;
        array_push($errorText, "Needs more K!");
    }

    $myData = [
        "input" => $input,
        "valid" => $valid,
        "errors" => $errorText
    ];
    return json_encode($myData);
});
$router->run();