var myView = {
    el: "firstView",
    url: "index.php/getData",
    dependencies: []
};

createView(myView);

var loginForm = document.getElementById("loginForm");

async function submitForm(event) {
    event.preventDefault();
    event.stopPropagation();
    var form = event.target;

    var username = form[0].value;
    var password = form[1].value;
    console.log(username, password);
    var response = await request("user.php/login/" + username + "/" + password);
    console.log(response);
    var loginWrapper = document.getElementById("login");
    if (response) {
        console.log("login success");
        loginWrapper.classList.add("animated");
        //loginWrapper.classList.add("hinge");
        loginWrapper.classList.add("lightSpeedOut");
        //loginWrapper.classList.add("zoomOutUp");
        //loginWrapper.classList.add("flipOutX");
    }
    else {
        console.log("nologin");
        loginWrapper.classList.add("animated");
        //loginWrapper.classList.add("shake");
        //loginWrapper.classList.add("swing");
        //loginWrapper.classList.add("tada");
        loginWrapper.classList.add("rubberBand");
    }
}
loginForm.addEventListener("submit", submitForm.bind(myView));

function logout() {
    window.location = "user.php/logout";
}
// bind helpers to form fields
enableFormValidation();
