/*
 * Suppress the default HTML 5 validation UI and replace it with custom CSS tooltips
 *
 */
async function handleValidation(event) {
    // suppress the default bubble
    event.preventDefault();
    // get input element
    let formField = event.target;

    /* select function with the name specified in the data-custom-validator prop
    *  and assign it to customValidator const
    *  each function represents a certain type of custom validation, and a local function with the
    *  customValidatorName needs to implemented or imported
    */
    const customValidatorName = formField.dataset.customValidator;
    // custom validation function result
    let customValidation = {};
    // a custom validator was selected in the data attribute
    if(customValidatorName)
    {
        const customValidator = window[customValidatorName];
        if (customValidator) {
            customValidation = await customValidator(formField);
        }
    }


    /* get the overall validity after resolving all constraints
    *  the validity field contains a validityState object where built-in constraints
    *  like required, minlength etc are resolved as well as custom constraints
    */
    const validity = formField.validity;
    // get the error message element corresponding to the input element
    let errorMessage = document.querySelector("#" + formField.id + "_error");

    if (errorMessage) {
        // if the constrained has been satisfied, remove the tooltip. else make the tooltip visible,
        if (formField.validity.valid) {
            delete errorMessage.dataset.balloonVisible;
        }
        else {
            errorMessage.dataset.balloonVisible = true;
            // if error messages were returned from the custom validator, overwrite the default ones with them
            if(customValidation.errors.length > 0 ) {
                errorMessage.dataset.balloon = customValidation.errors.join("\n");
            }

        }
    }

    return validity.valid;
}

/*
 * Validator function
 * @param {obj} inputElement The form element to be validated
 * The function performs any type of custom validation and then calls
 * setCustomValidity on the formField to indicate whether the validation succeeded or failed.
 * Calling setCustomValidity with ANY string value invalidates the field. Calling it with an empty string validates the field.
 */
async function validator1(formField) {

    let validation = await request("index.php/validate/" + formField.value, "");
    if (!validation.valid) {
        formField.setCustomValidity("fail");
    }
    else {
        formField.setCustomValidity("");
    }

    return validation;
}

async function validator2(formField) {
    if (formField.value.length > 3) {
        let validation = await request("index.php/validate/" + formField.value, "");

        if (!validation.valid) {
            formField.setCustomValidity("fail");
        }
        else {
            formField.setCustomValidity("");
        }

        return validation;
    }
}

/*
 *
 * Loop over form elements and bind event handlers to form elements we want to validate
 */
function enableFormValidation() {
    const inputElements = document.querySelectorAll("input,select,textarea,checkbox");

// loop over all input elements and attach event listeners
    for (let i = 0; i < inputElements.length; i++) {
        // called whenever the input constraints of the form field have been validated
        // this happens when a wrong value is entered, or when the form is submitted with no value entered
        inputElements[i].addEventListener("invalid", function (event) {
            handleValidation(event);
        });

        // called on every keystroke to check if the constraint has been fulfilled
        inputElements[i].addEventListener("keyup", function (event) {
            handleValidation(event);
        });
        // called on every keystroke to check if the constraint has been fulfilled
        inputElements[i].addEventListener("change", function (event) {
            handleValidation(event);
        });
    }
}

/*
 * Toggle opening state of progress buttons
 *
 */
function toggleIndicatorAnimation(loadingIndicator) {
    // add animation
    if (loadingIndicator.classList.contains("animated-loader-expanded")) {
        // remove old state base class
        loadingIndicator.classList.remove("animated-loader-expanded");
        // add base class
        loadingIndicator.classList.add("animated-loader");
        // add animation
        loadingIndicator.classList.add("animated-loader-animateCollapse");
    }
    else {
        // remove old state
        loadingIndicator.classList.remove("animated-loader");
        // add base class
        loadingIndicator.classList.add("animated-loader-expanded");
        // add animation
        loadingIndicator.classList.add("animated-loader-animateExpand");
    }

    // register event handler to remove the CSS transition once it is completed
    // this way we can trigger the animation over and over
    loadingIndicator.addEventListener("transitionend", function (event) {
        // only react to events pertaining to our indicator
        if (event.target.className == "loadingIndicator") {
            // remove old animations
            if (loadingIndicator.classList.contains("animated-loader")) {
                loadingIndicator.classList.remove("animated-loader-animateCollapse");
            }
            if (loadingIndicator.classList.contains("animated-loader-expanded")) {
                loadingIndicator.classList.remove("animated-loader-animateExpand");
            }
        }

    });
}
function toggleLoadingIndicator(parentViewId) {
    let loadingIndicator = document.getElementById("progress_" + parentViewId);
    if (loadingIndicator) {
        window.setTimeout(function () {
            toggleIndicatorAnimation(loadingIndicator);
        }, 1);
    }
}
function request(endpoint, loadingIndicatorElement) {
    if (loadingIndicatorElement) {
        toggleLoadingIndicator(loadingIndicatorElement);
    }

    return fetch(endpoint)
        .then(response => {
            if (loadingIndicatorElement) {
                toggleLoadingIndicator(loadingIndicatorElement);
            }
            return isOk(response);
        })
        .then(response => response.json());
}

function isOk(response) {
    if (response.ok) {
        return Promise.resolve(response);
    }

    throw new Error('Network response was not ok');
}
/*
 * Function to create Vue.js view.
 * It binds together an HTML element with a datasource reactively
 * @param Object myView An object representing the HTML element to be bound
 */
async function createView(myView) {
    /*
     * Object representing the data model for the view
     * @param Array items An array representing the data to be rendered in the view
     * @param Array pages An array representing the pages for the current datasource url, given pagination parameters
     * @param int limit Represents the database LIMIT parameter for the current page of data
     * @param int offset Represents the database OFFSET parameter for the current page of data
     * @param Array dependencies A list of Vue.js views that depend on the data in  this view, and should be updated if this view is updated
     * @param String url The data URL that delivers the models items
     */
    var myModel = {
        items: [],
        additionalData: [],
        offset: 0,
        currentPage: 1,
        itemsCount: 0,
        url: myView.url,
        dependencies: myView.dependencies
    };

    /* Create the Vue.js view given the element myEl and the data in myModel
     * @param string el The DOM ID of the element to bind the view to
     * @param obj data The JSON object representing the data
     * @param obj methods Method the view exposed
     * @method function afterUpdate(updateHandler) Function to be called on the view. The closure passed in is executed
     * after the data model has been updated and the DOM rendering has finished
     */
    var vueview = new Vue({
        el: "#" + myView.el,
        data: myModel,
        methods: {
            update: function (updateDependencies) {
                updateView(this, updateDependencies);
            },
            afterUpdate: function (updateHandler) {
                this.$on("iloaded", function () {
                    Vue.nextTick(updateHandler);
                });
            },
            afterLoad: function (updateHandler) {
                this.$once("iloaded", function () {
                    Vue.nextTick(updateHandler);
                });
            }
        }
    });

    const response = await request(myModel.url, myView.el);

    myModel.items = response.items;

    //total number of items available
    myModel.itemsCount = response.count;

    //if the response has additional data fields, write em to the additionalFields field
    if (response.additionalData != undefined) {
        myModel.additionalData = response.additionalData;
    }

    //emit an event that indicates the view has finished loading data
    vueview.$emit("iloaded");

    //return the view
    return vueview;
}

/*
 * Function to recursively update a view and its dependencies
 * @param Object view A vue.js view to be updated
 * @param boolean updateDependencies A bool to determine whether dependencies should be updated (default = true)
 */
async function updateView(view, updateDependencies) {
    //Default update dependencies to true
    if (updateDependencies === undefined) {
        updateDependencies = true;
    }

    //get the URL from the view and modify it to add the limit and offset for the DB query
    var myUrl = view.url;
    if (view.limit > 0) {
        myUrl += "&limit=" + view.limit
    }
    if (view.offset > 0) {
        myUrl += "&offset=" + view.offset;
    }

    const response = await request(myUrl, view.$el.id);

    Vue.set(view, "items", response.items);
    //if the response has additional data fields, write em to the additionalFields field
    if (response.additionalData != undefined) {
        Vue.set(view, "additionalData", response.additionalData);
    }

    view.$emit("iloaded");
    //update dependencies
    if (updateDependencies == true) {
        //get the array of dependendant views
        const viewsToUpdate = view.$get("dependencies");
        if (viewsToUpdate.length > 0) {
            for (var i = 0; i < viewsToUpdate.length; i++) {
                //load the same sub pages for dependant views that are loaded for the root view
                Vue.set(viewsToUpdate[i], "limit", view.limit);
                Vue.set(viewsToUpdate[i], "offset", view.offset);
                //recursive call
                updateView(viewsToUpdate[i], true);
            }
        }
    }
}