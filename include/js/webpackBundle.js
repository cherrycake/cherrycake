/*
 * This file imports all the JS files to be bundled by wepback
 * they will be bundled into the cherrycake.js bundle
 *
 */
// import "./components/loadingIndicatorComponent.js";
import loader from "../../templates/components/loader.vue";
Vue.component("loader", loader);

import indexList from "../../templates/components/indexList.vue";
Vue.component("indexList", indexList);